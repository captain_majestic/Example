
public class C {
    public static void m1() {
        m2();
    }

    public static void m2() {
        // will be modified
        new Object();
    }

    public static void m3() {
    }
}

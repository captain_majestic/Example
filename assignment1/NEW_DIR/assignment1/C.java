package assignment1;
public class C {
	
	int param = 0;
	
	public C(){
		add(1);
	}
	
	public C(int i){
		param = i;
	}
	
	public int get(){
		return param;
	}
	
	public void add(int i){
		param = get() + i;
	}

}


public class B {
    public int b1(String s1, String s2) {
        if (s1.equals(s2)) {
            return 22;
        }
        int c = s1.length() + s2.length();
        return c;
    }

    public int[] sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] < array[j]) {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
        return array;
    }
}
